﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using ManagePeople;
using LiteDB;
using System.IO;

namespace ManagePeopleMobile
{
    public class Model
    {
        private readonly string DB_PATH; //= Environment.getfolderpath + "/ManagePeople";
        private const string DB_NAME = "LocalData.db";

        private string DB_FULL_NAME { get
            {
                return Path.Combine(DB_PATH + DB_NAME);
            }
        }

        public User user { get; set; }

        public Model ()
        {
            DB_PATH = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "ManagePeople");

            if (!Directory.Exists(DB_PATH))
                Directory.CreateDirectory(DB_PATH);

            using (var db = new LiteDatabase(DB_FULL_NAME))
            {
                var col = db.GetCollection<User>("CurrentUser");

                if (col.Count() > 0) 
                    user = col.FindOne(Query.All());
            }
        }
    }
}