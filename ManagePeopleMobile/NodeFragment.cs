﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ManagePeople;

namespace ManagePeopleMobile
{
    public class NodeFragment : Fragment
    {
        private OrganizationNode node;

        public NodeFragment(OrganizationNode node): base()
        {
            this.node = node;
        }

        public string Name => node.name;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            //return inflater.Inflate(Resource.Layout.tab_content, container, false);
            var view = inflater.Inflate(Resource.Layout.tab_content, container, false);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = node.name;

            return view;
            //return base.OnCreateView(inflater, container, savedInstanceState);
        }

        /*public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            TextView text = View.FindViewById<TextView>(Resource.Id.textView1);
            text.Text = node.name;
        }*/
    }
}