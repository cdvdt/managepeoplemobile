﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using ManagePeople;

namespace ManagePeopleMobile
{
    public class Controller
    {
        private Model model;
        
        public Controller()
        {
            model = new Model();
        }

        public User getCurrentUser()
        {
            return model.user;
        }
    }
}