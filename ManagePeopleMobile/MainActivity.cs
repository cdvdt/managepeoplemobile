﻿using System;
using System.Collections.Generic;
using Android;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Lang;
using ManagePeople;
using static Android.Support.V4.Widget.DrawerLayout;

namespace ManagePeopleMobile
{
    public class NodeFragmentAdapter : FragmentPagerAdapter
    {
        private List<NodeFragment> nodes = new List<NodeFragment>();

        public NodeFragmentAdapter(Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
        }

        public void addTab(NodeFragment newTab)
        {
            nodes.Add(newTab);
            NotifyDataSetChanged();
        }

        public override int Count => nodes.Count;

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            return nodes[position];
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(nodes[position].Name);
        }
    }

    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        private Controller controller;

        private void UpdateUserInfo()
        {
            TextView username = FindViewById<TextView>(Resource.Id.Username);
            TextView userLogin = FindViewById<TextView>(Resource.Id.UserLogin);

            User currentUser = controller.getCurrentUser();

            if (currentUser == null)
            {
                username.Text = "Sign in";
                userLogin.Text = "Sign in";
            }
            else
            {
                username.Text = currentUser.username;
                userLogin.Text = currentUser.login;
            }
        }

        private void AddTab(string tabText, Android.Support.V4.App.Fragment fragment)
        {
            //var tab = this.ActionBar.NewTab();
            /*var tabs = FindViewById<TabLayout>(Resource.Id.tabs);
            var tab = tabs.NewTab();
            tab.SetText(tabText);

            /*tabs.TabSelected += delegate (object sender, TabLayout.TabSelectedEventArgs e)
            {
                e. FragmentTransaction.Replace(Resource.Id.fragmentConteiner, fragment);
            };
            this.ActionBar.AddTab(tab);*/
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            Android.Support.V7.App.ActionBarDrawerToggle toggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            drawer.DrawerSlide += delegate (object s, DrawerSlideEventArgs e) { UpdateUserInfo(); };

            NodeFragmentAdapter adapter = new NodeFragmentAdapter(SupportFragmentManager);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.pager);
            viewPager.Adapter = adapter;

            /*TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.tabs);
            tabLayout.SetupWithViewPager(viewPager);*/

            var node = new OrganizationNode();
            node.name = "Nó inicial";

            adapter.addTab(new NodeFragment(node));
            node = new OrganizationNode();
            node.name = "Segundo Nó";

            adapter.addTab(new NodeFragment(node));

            controller = new Controller();

            /*AddTab("Root", new NodeFragment());
            AddTab("Test 1", new NodeFragment());
            AddTab("Test 2", new NodeFragment());
            AddTab("Test 3", new NodeFragment());
            AddTab("Test 4", new NodeFragment());
            AddTab("Test 5", new NodeFragment());*/
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if(drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.navOrganizationRoot)
            {
                // Handle the camera action
            }
            else if (id == Resource.Id.navMyDepartment)
            {

            }
            else if (id == Resource.Id.navMembers)
            {

            }
            else if (id == Resource.Id.navSubDepartments)
            {

            }
            else if (id == Resource.Id.navConfigurations)
            {
                UpdateUserInfo();
            }
            else if (id == Resource.Id.navSyncronize)
            {

            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }
    }
}

